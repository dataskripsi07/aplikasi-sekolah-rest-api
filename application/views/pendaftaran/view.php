  <div class="card">
            <div class="card-body">
                <div class="row mb-4">
                    <div class="col">
                        
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
                <div class="table-responsive">
                    <table id="exampleDataTable" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Pendaftaran</th>
                                <th>Nama Lengkap</th>
                                <th>Nama Ayah</th>
                                <th>Nama Ibu</th>
                                <th>Tanggal Daftar</th>
                                <th>Status</th>
                                <th>Periode</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        <tbody><?php
                        $no = 1;
                        $lulus = "";
                        $where = "";
                        if ($this->session->userdata('level') == 'siswa') {
                            $id_pendaftaran =  $this->session->userdata('id_user');
                            $where = "WHERE a.id_pendaftaran='$id_pendaftaran' ";
                        }
                        $sql = "SELECT a.*, b.id_user FROM pendaftaran as a INNER JOIN users as b ON a.id_pendaftaran=b.id_pendaftaran $where";
                        $pendafataran_data = $this->db->query($sql);
                        foreach ($pendafataran_data->result() as $pendaftaran)
                        {
                            ?>
                            <tr>
            <td width="80px"><?php echo $no ?></td>
            <td><?php echo "PSB".$pendaftaran->id_pendaftaran ?></td>
            <td><?php echo $pendaftaran->nama ?></td>
            <td><?php echo $pendaftaran->nama_ayah ?></td>
            <td><?php echo $pendaftaran->nama_ibu ?></td>
            <td><?php echo $pendaftaran->created_at ?></td>
            <td>
                <?php 
                $title_lulus = "";
                $get_lulus = "";

                $this->db->where('id_user', $pendaftaran->id_user);
                $check = $this->db->get('hasil_seleksi_administrasi');
                if ($check->num_rows() > 0) {
                    $lulus_administrasi = 'ya';
                    $get_lulus = '?lulus=t';
                    $title_lulus = "<b style='color: green'>Lulus Administrasi</b>";
                } else {
                    $lulus_administrasi = "tidak";
                    $title_lulus = "<b>Proses</b>";
                     $get_lulus = '?lulus=notset';
                }

                $this->db->where('id_user', $pendaftaran->id_user);
                $check = $this->db->get('hasil_seleksi');
                if ($check->num_rows() > 0) {
                    $lulus = 'ya';
                    $title_lulus = "<b style='color: green'>Lulus</b>";
                    $get_lulus = '?lulus=y';
                } else {
                    $lulus = "tidak";
                }
                echo $title_lulus;

                 ?>         
            </td>
            <td>
                <?php echo get_data('periode','id_periode',$pendaftaran->id_periode, 'periode') ?>
            </td>
            <td style="text-align:center" width="200px">

                        <a href="Pendaftaran/lihat/<?php echo $pendaftaran->id_pendaftaran.$get_lulus ?>" title="Update Data" class="btn btn-sm btn-primary">Lihat
                        </a>
                        <?php if ($this->session->userdata('level') == 'admin'): ?>
                            <a href="Pendaftaran/delete/<?php echo $pendaftaran->id_pendaftaran ?>" title="Hapus Data" onclick="javasciprt: return confirm('Yakin akan hapus data ini ?')" class="btn btn-sm btn-danger"><i class="bx bx-trash-alt me-0"></i>
                            </a>

                            <?php if ($lulus_administrasi == 'tidak'): ?>
                                <a href="Pendaftaran/approve_administrasi/<?php echo $pendaftaran->id_pendaftaran ?>" title="Lulus Administrasi" onclick="javasciprt: return confirm('Yakin akan set lulus administrasi siswa ini ?')" class="btn btn-sm btn-warning">Lulus Administrasi</i>
                                </a>
                            <?php endif ?>
                            <?php if ($lulus == 'tidak'): ?>
                                <a href="Pendaftaran/approve/<?php echo $pendaftaran->id_pendaftaran ?>" title="Lulus" onclick="javasciprt: return confirm('Yakin akan set lulus siswa ini ?')" class="btn btn-sm btn-success">Lulus</i>
                                </a>
                            <?php endif ?>

                            
                            
                        <?php endif ?>
                        
            
            </td>
        </tr>
                            <?php
                            $no++;
                        }
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    