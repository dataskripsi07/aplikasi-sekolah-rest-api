<?php 
$detail = $this->db->get_where('pendaftaran', ['id_pendaftaran' => $this->uri->segment(3)])->row();
 ?>
<div class="card">
    <div class="card-body">
        
        <table class="table table-hover table-striped">
            <tr>
                <td>No Pendaftaran</td>
                <td>: ONLINE<?php echo $detail->id_pendaftaran ?></td>
            </tr>
            
            <?php if ($this->input->get('lulus') == 't' OR $this->input->get('lulus') == 'y'): ?>
                <tr>
                <td>Nama Lengkap</td>
                <td>: <?php echo $detail->nama ?></td>
            </tr>
            <tr>
                <td>Tanggal Test</td>
                <td>: <?php echo substr($detail->created_at, 0,10) ?></td>
            </tr>
            <?php endif ?>
            
            <?php if ($this->input->get('lulus') == 'notset'): ?>
            <tr>
                <td>Nama Lengkap</td>
                <td>: <?php echo $detail->nama ?></td>
            </tr>
            <tr>
                <td>Tanggal Lahir</td>
                <td>: <?php echo $detail->tanggal_lahir ?></td>
            </tr>
            <tr>
                <td>Jenis Kelamin</td>
                <td>: <?php echo $detail->jenis_kelamin ?></td>
            </tr>
            <tr>
                <td>Agama</td>
                <td>: <?php echo $detail->agama ?></td>
            </tr>
            <tr>
                <td>Nama Ayah</td>
                <td>: <?php echo $detail->nama_ayah ?></td>
            </tr>
            <tr>
                <td>No Telp Ayah</td>
                <td>: <?php echo $detail->no_telp_ayah ?></td>
            </tr>
            <tr>
                <td>Nama Ibu</td>
                <td>: <?php echo $detail->nama_ibu ?></td>
            </tr>
            <tr>
                <td>No Telp Ibu</td>
                <td>: <?php echo $detail->no_telp_ibu ?></td>
            </tr>
            <tr>
                <td>Foto</td>
                <td>: <a href="image/foto/<?php echo $detail->foto ?>" target="_blank">Lihat Foto</a></td>
            </tr>
            <tr>
                <td>Foto KK</td>
                <td>: <a href="image/foto/<?php echo $detail->foto_kk ?>" target="_blank" >Lihat Foto</a></td>
            </tr>
            <tr>
                <td>Foto AKTE</td>
                <td>: <a href="image/foto/<?php echo $detail->foto_akte ?>" target="_blank" >Lihat Foto</a></td>
            </tr>
            <?php endif ?>
        </table>

        <?php if ($this->input->get('lulus') == 'y'): ?>

            <center>
                <h5 style="color: green;"> Selamat anda telah terdaftar sebagai siswa SDN 011/XI Desa Gedang tahun ajaran 2023 </h5>
            </center>
            
        <?php endif ?>
    </div>
</div>
        
    