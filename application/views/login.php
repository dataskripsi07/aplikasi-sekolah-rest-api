<!doctype html>
<html lang="en">

<head>
     <!-- Required meta tags -->
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <base href="<?php echo base_url() ?>">
     <!--favicon-->
     <link rel="icon" href="assets/logo.png" type="image/png" />
     <!--plugins-->
     <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
     <link href="assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
     <link href="assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
     <!-- loader-->
     <link href="assets/css/pace.min.css" rel="stylesheet" />
     <script src="assets/js/pace.min.js"></script>
     <!-- Bootstrap CSS -->
     <link href="assets/css/bootstrap.min.css" rel="stylesheet">
     <link href="assets/css/bootstrap-extended.css" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;display=swap" rel="stylesheet">
     <link href="assets/css/app.css" rel="stylesheet">
     <link href="assets/css/icons.css" rel="stylesheet">
     <title>Pendaftaran Sekolah - Login</title>
</head>

<body class="bg-login">
     <!--wrapper-->
     <div class="wrapper">
          <div class="section-authentication-signin d-flex align-items-center justify-content-center my-5 my-lg-0">
               <div class="container-fluid">
                    <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                         <div class="col mx-auto">
                              <div class="mb-4 text-center">
                                   <img src="image/sd.png" width="180" alt="" />
                              </div>
                              <div class="card">
                                   <div class="card-body">
                                        <div class="border p-4 rounded">
                                             <div class="text-center">
                                                  <h3>Sign in</h3>
                                                  <p>
                                                       Silahkan Login disini.
                                                  </p>
                                             </div>
                                             <?php if (isset($_GET['username'])): ?>
                                                  <p>
                                                       <h5>Username : <?php echo $this->input->get('username') ?></h5>
                                                       <h5>Password : <?php echo $this->input->get('username') ?></h5>
                                                  </p>
                                             <?php endif ?>
                                             
                                             <div class="form-body">
                                                  <form class="row g-3" action="Login/auth" method="POST">
                                                       <div class="col-12">
                                                            <label for="inputEmailAddress" class="form-label">Username</label>
                                                            <input type="text" class="form-control"
                                                                 id="inputEmailAddress" name="username" placeholder="Username" autofocus>
                                                       </div>
                                                       <div class="col-12">
                                                            <label for="inputChoosePassword" class="form-label">Enter
                                                                 Password</label>
                                                            <div class="input-group" id="show_hide_password">
                                                                 <input type="password"
                                                                      name="password"
                                                                      class="form-control border-end-0"
                                                                      id="inputChoosePassword" value=""
                                                                      placeholder="Enter Password"> <a
                                                                      href="javascript:;"
                                                                      class="input-group-text bg-transparent"><i
                                                                           class='bx bx-hide'></i></a>
                                                            </div>
                                                       </div>
                                                       
                                                       <div class="col-12">
                                                            <div class="d-grid">
                                                                 <button type="submit" class="btn btn-primary"><i
                                                                           class="bx bxs-lock-open"></i>Login</button>
                                                                           <br>
                                                                 <a href="Pendaftaran" class="btn btn-info">Daftar</a>
                                                            </div>
                                                       </div>
                                                  </form>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
                    <!--end row-->
               </div>
          </div>
     </div>
     <!--end wrapper-->
     <!-- Bootstrap JS -->
     <script src="assets/js/bootstrap.bundle.min.js"></script>
     <!--plugins-->
     <script src="assets/js/jquery.min.js"></script>
     <script src="assets/plugins/simplebar/js/simplebar.min.js"></script>
     <script src="assets/plugins/metismenu/js/metisMenu.min.js"></script>
     <script src="assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>
     <!--Password show & hide js -->
     <script>
     $(document).ready(function() {
          $("#show_hide_password a").on('click', function(event) {
               event.preventDefault();
               if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("bx-hide");
                    $('#show_hide_password i').removeClass("bx-show");
               } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("bx-hide");
                    $('#show_hide_password i').addClass("bx-show");
               }
          });
     });
     </script>
     <!--app JS-->
     <script src="assets/js/app.js"></script>
     <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
     <script type="text/javascript"><?php echo $this->session->userdata('pesan') ?></script>
</body>

</html>