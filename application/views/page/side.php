<div class="sidebar-wrapper" data-simplebar="true">
     <div class="sidebar-header">
          <div>
               <img src="image/sd.png" style="widht: 60px;height: 30px;" alt="logo icon">
          </div>
          <div>
               <!-- <h4 class="logo-text" style="color: #CC9C27">Zam Zam</h4> -->
          </div>
          <div class="toggle-icon ms-auto" style="color: #CC9C27"><i class='bx bx-arrow-to-left'></i>
          </div>
     </div>
     <!--navigation-->
     <ul class="metismenu" id="menu">
          <li class="menu-label">Main Menu</li>
          <li>
               <a href="app">
                    <div class="parent-icon"><i class='bx bx-home-circle'></i>
                    </div>
                    <div class="menu-title">Dashboard</div>
               </a>
          </li>
          
		<?php if($this->session->userdata('level') == 'admin'): ?>
		<li>
               <a href="Pendaftaran/view">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Pendaftaran</div>
               </a>
          </li>
          <li>
               <a href="kelulusan_administrasi">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Kelulusan Administrasi</div>
               </a>
          </li>
          <li>
               <a href="kelulusan">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Diterima</div>
               </a>
          </li>
          <li>
               <a href="periode">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Periode</div>
               </a>
          </li>
          <li>
               <a href="app/users">
                    <div class="parent-icon"><i class='bx bx-user-circle'></i>
                    </div>
                    <div class="menu-title">Data User</div>
               </a>
          </li>
          <?php else: ?>
          <li>
               <a href="Pendaftaran/view">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Pendaftaran</div>
               </a>
          </li>
          <li>
               <a href="kelulusan_administrasi">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Kelulusan Administrasi</div>
               </a>
          </li>
          <li>
               <a href="kelulusan">
                    <div class="parent-icon"><i class='bx bx-file'></i>
                    </div>
                    <div class="menu-title">Data Kelulusan</div>
               </a>
          </li>
          <?php endif ?>
     </ul>
     <!--end navigation-->
</div>